# Report Card Product

This product provides teachers with an intuitive experience for streamlining the report card management process.

## Workflows

- [Planning](https://gitlab.com/tech-marketing/demos/gitlab-agile-demo/initech/music-store/1/-/boards/1503756?&label_name[]=product%3A%3A%20report%20card)
- [Validation](https://gitlab.com/tech-marketing/demos/gitlab-agile-demo/initech/music-store/1/-/boards/1503859?&label_name[]=type%3A%3Afeature)
- [Triage](https://gitlab.com/tech-marketing/demos/gitlab-agile-demo/initech/music-store/1/-/boards/1503776?milestone_title=No+Milestone&&label_name[]=product%3A%3A%20report%20card)
- [Current Sprint](https://gitlab.com/tech-marketing/demos/gitlab-agile-demo/initech/music-store/1/-/boards/1503858?milestone_title=Sprint%202020-01-31&)
- [Deploy & Measure](https://gitlab.com/tech-marketing/demos/gitlab-agile-demo/initech/music-store/1/-/boards/1507724?&label_name[]=product%3A%3A%20report%20card)

## FY21 Themes

- [Create lovable experiences](https://gitlab.com/groups/tech-marketing/demos/gitlab-agile-demo/initech/music-store/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=theme%3A%3Alovable)
- [Increase Serviceable Addressable Market](https://gitlab.com/groups/tech-marketing/demos/gitlab-agile-demo/initech/music-store/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=theme%3A%3Aincrease%20sam)
- [Decrease costs through operating efficiencies](https://gitlab.com/groups/tech-marketing/demos/gitlab-agile-demo/initech/music-store/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=theme%3A%3Areduce%20costs)

## Important Pages

- [Teacher Services Strategic Direction](https://gitlab.com/tech-marketing/demos/gitlab-agile-demo/initech/music-store/director-group)
- [Initech Strategic Direction](https://gitlab.com/tech-marketing/demos/gitlab-agile-demo/initech/executive-leadership/-/wikis/CY20-Strategic-Planning)